var processCSV = require('./lib/process-csv');
var async = require('async');
var fs = require('fs');
var _ = require('lodash');

var argv = require('minimist')(process.argv.slice(2));
var inFile = argv._[0];
var outFile = argv._[1];

if (!inFile || !outFile) {
  console.log('Must provide path to source and destination files!');
  console.log('e.g. node app.js in.csv out.csv');
  process.exit(1);
}

var cells = {};

async.waterfall([
  function readAndProcessCSV(cb) {
    console.log('Processing ', inFile);
    fs.access(inFile, fs.F_OK, function(err) {
      if (err) {
        console.log('Path to ' + inFile + ' cannot be accessed!');
        cb(err);
      } else {
        processCSV.loadCSV(inFile, cells, cb);
      }
    });
  },
  function writeCSV(csv, cb) {
    console.log('Processed CSV, writing to ', outFile);
    processCSV.writeCSV(outFile, cells, cb);
  }
], function(err) {
  if (err) {
    console.log('Problem encountered: ', err);
  } else {
    console.log('Processed spreadsheet is saved, please check ', outFile);
  }
});
