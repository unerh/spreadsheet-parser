## Overview

Task description

Write a program to which parses a spreadsheet-like CSV file and evaluates each cell by
these rules:

1. Each cell is an expression in postfix notation (see Wikipedia).
2. Each token in the expression will be separated by one or more spaces.
3. One cell can refer to another cell with the {LETTER}{NUMBER} notation (e.g. “A2”,
   “B4”– letters refer to columns, numbers to rows).
4. Expressions may include the basic arithmetic operators `+, -, *, /`

Your program should output another CSV file of the same dimensions containing the results
of evaluating each cell to its final value. If any cell is an invalid expression, then for that cell
only print #ERR.

## Requirements

* Node v4.2.3 (If nvm is installed on target machine a .nvmrc file is provided)
* Requires Mocha for testing (can be installed as per setup below)

## Setup

```
nvm install
npm install -g mocha
npm install
npm test
```

## Instructions

After following the setup instructions above, the application can be executed as follows:

```
* node app.js path-to-input-file path-to-output-file.
* e.g.  node app.js ./test/test.csv output.csv
```

## Assumptions & Limitations

* Input file is a valid CSV file, i.e same number of columns should exist for each row, even
  if the cells have empty values. Checks are not made for this.
* No paranthesis are allowed within an atomic postfix expression
* Any cells referenced within other cells, which haven't been defined in the input file, 
  is assumed to be invalid and will therefore have value #ERR
* Any cell that references another cell with #ERR value will automatically be set to #ERR too
* This application only supports columns from A-Z and unlimited rows (subject to memory availability).

## Solution

This is a basic solution to the given problem, where an object dictionary is used to capture both
expressions and evaluations of the cells in a two-dimension spreadsheet. Cells that do not contain
references to other cells are evaluated as the file is loaded to speed up the processing/traversal
of the structure post initialising.

Regular expressions are used to identify cell references and the referenced cells are recurssed during
the traversal and evaluated first. Every reference is then replaced with the result of that cell in the
original expression, and the expression it self is then evaluated.

The main shortcoming with this solution would be due to the use of recurssion, it may not handle a very
large spreadsheet with deep nested cell references because of stack issues.

