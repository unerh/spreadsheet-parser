var traverseCells = require('./traverse-cells');
var calcPostfix = require('./calc-postfix');
var fs = require('fs');
var readline = require('readline');
var async = require('async');

var alphas = 'abcdefghijklmnopqrstuvwxyz'.split('');

module.exports = { 

  /** Reads CSV file and initialises the cells object for further processing */
  loadCSV: function(fileIn, cells, done) {
    var rl = readline.createInterface({
      input: fs.createReadStream(fileIn)
    });

    var rowCount = 1;

    rl.on('line', function(row) {
      var columns = row.split(',');

      for (var i = 0; i < columns.length; i++) {
        if (typeof cells[alphas[i]] === 'undefined') {
          cells[alphas[i]] = {};
        }

        // evaluate cell if it doesn't contain cell reference
        // this is to take advantage of the file read and to
        // increase performance in the case of very large files
        if (columns[i].search(/([a-z]\d*)+/gi) < 0) {
          var cellVal = calcPostfix(columns[i]);
          cellVal = (cellVal === null) ? '#ERR' : cellVal;
          cells[alphas[i]][rowCount] = { 'expr': columns[i], 'result': cellVal };
        } else {
          cells[alphas[i]][rowCount] = { 'expr': columns[i] };
        }
      }

      rowCount++;
    }).on('close', function () {
      
      async.forEachOf(cells, function(rows, col, cb) {
        async.forEachOf(rows, function(value, row, cb) {
          if (typeof value.result === 'undefined') {
            traverseCells(cells, col, row);
          }
          cb();
        }, function (err) {
        });
        cb();
      }, function (err) {
        if (err) console.error(err.message);

        cells.rowCount = rowCount - 1;
        done(null, cells);
      });
    });
  },

  writeCSV: function(fileOut, cells, done) {

    var rowCount = cells.rowCount;
    var rowStr = '';
    var i, j;

    for (j = 1; j <= rowCount; j++) {
      // traverse the alphabet till no matching column is found
      i = 0;
      while (cells[alphas[i]]) {
        rowStr += cells[alphas[i]][j].result + ',';
        i++;
      }

      // remove last delimeter and add new line
      rowStr = rowStr.substring(0, rowStr.length - 1);
      rowStr += '\n';
    }

    fs.writeFileSync(fileOut, rowStr);
    done(null, cells);
  }

};
