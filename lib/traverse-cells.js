var _ = require('lodash');
var calcPostfix = require('./calc-postfix');

module.exports = function traverse(cells, col, row) {

  if (cells && cells[col] && cells[col][row]) {
    var cell = cells[col][row];
    // calculate result if it doesn't already exist
    if (typeof cell.result === 'undefined') {
      // process cell
      var expr = cell.expr;
      // check for referenced cells in the expression
      var refCells = expr.match(/([a-z]\d*)+/gi);

      if (refCells) {
        refCells = _.uniqBy(refCells, String.toLowerCase);

        for (var i = 0; i < refCells.length; i++) {
          var colRow = refCells[i].match(/[a-z]+|\d+/g);
          var refVal = traverse(cells, colRow[0], colRow[1]);
          cell.expr = cell.expr.replace(RegExp(refCells[i], 'gi'), refVal);
        }
      }

      var calcExpr = calcPostfix(cell.expr);
      cell.result = (calcExpr === null) ? '#ERR' : calcExpr;
    }
    return cell.result;
  } else {
    return '#ERR';
  }
};
