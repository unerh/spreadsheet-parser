
module.exports = function(expression) {

  if (typeof expression === 'undefined' || typeof expression !== 'string') {
    return null;
  } else if (expression.length === 0) {
    return 0;
  }

  // get the tokens
  var tokens = expression.trim().split(/\s+/);
  var stack = [];

  // check for invalid characters
  if (expression.match(/[^\d+\-*/.\s]/)) {
    return null;
  } else if (expression.match(/^\s+$/) ) {
    // return 0 for an empty expression
    return 0;
  }

  var i = 0, token, op1, op2;
  // traverse tokens and apply algorithm as per Wikipedia postfix entry
  for (; i < tokens.length; i++) {
    token = tokens[i];

    if (!isNaN(token)) {
      stack.push(Number(token));
      tokens.splice(i--, 1);
    } else if (token === '+' && stack.length >= 2) {
      op2 = stack.pop();
      op1 = stack.pop();
      stack.push(op1 + op2);
      tokens.splice(i--, 1);
    } else if (token === '-' && stack.length >= 2) {
      op2 = stack.pop();
      op1 = stack.pop();
      stack.push(op1 - op2);
      tokens.splice(i--, 1);
    } else if (token === '*' && stack.length >= 2) {
      op2 = stack.pop();
      op1 = stack.pop();
      stack.push(op1 * op2);
      tokens.splice(i--, 1);
    } else if (token === '/' && stack.length >= 2) {
      op2 = stack.pop();
      op1 = stack.pop();
      stack.push(op1 / op2);
      tokens.splice(i--, 1);
    }
  }

  var result = null;
  // ensuring no unused operand or operator is remaining
  if (stack.length === 1 && tokens.length === 0) {
    result = stack.pop();
  }

  return result;
};

