var assert = require('assert');
var async = require('async');
var calcPostfix = require('../lib/calc-postfix');
var traverseCells = require('../lib/traverse-cells');
var loadCSV = require('../lib/process-csv').loadCSV;
var writeCSV = require('../lib/process-csv').writeCSV;
var fs = require('fs');


// --------- calc-postfix tests ------------

describe('Calculate Postfix invalid characters check', function() {
  it('Should return null for alphabets', function() {
    assert.equal(calcPostfix('a B +'), null);
  });

  it('should return null for non-arithmetic operators', function() {
    assert.equal(calcPostfix('3 4 &'), null);
    assert.equal(calcPostfix('3 ^ 5'), null);
  });

});

describe('Calculate Postfix invalid number of values check', function() {
  it('Should return null for insufficient number of values', function() {
    assert.equal(calcPostfix('+'), null);
    assert.equal(calcPostfix('3 4 - /'), null);
    assert.equal(calcPostfix('3 *'), null);
  });

  it('Should return null for too many values entered', function() {
    assert.equal(calcPostfix('1 2 '), null);
    assert.equal(calcPostfix('3 4'), null);
    assert.equal(calcPostfix('3 5 4 +'), null);
    assert.equal(calcPostfix('3 5 4 + 2 7 - /'), null);
  });

});

describe('Calculate Postfix valid expressions check', function() {
  it('Should return 0 for an empty input', function() {
    assert.equal(calcPostfix(''), 0);
    assert.equal(calcPostfix('  '), 0);
  });
  it('Should return value for a single value', function() {
    assert.equal(calcPostfix('3'), 3);
  });
  it('Should return 14 for \'5 1 2 + 4 * + 3 -\' (with various amount of spacing)', function() {
    assert.equal(calcPostfix('5 1  2 +  4 *  + 3 -'), 14);
    assert.equal(calcPostfix('5 1 2 + 4 * + 3 - '), 14);
  });
  it('Should return -13 for \'2 5 3 * -\' (testing for negative result)', function() {
    assert.equal(calcPostfix('2 5 3 * -'), -13);
  });
  it('Should return 3.5 for \'7 2 /\' (testing for decimal point result)', function() {
    assert.equal(calcPostfix('7 2 /'), 3.5);
    assert.equal(calcPostfix(' 7 2 /'), 3.5);
  });
});


// --------- traverse-cells tests ------------

describe('Traverse cells that reference #ERR values', function() {
  it('Should return #ERR for a1 and a2', function() {
    var cells = {
      a: {
        1: { expr: 'a2 4 +' },
        2: { expr: '+' }
      }
    };
    traverseCells(cells, 'a', 1);
    assert.equal(cells.a['1'].result, '#ERR');
    assert.equal(cells.a['2'].result, '#ERR');
  });
});

describe('Traverse cells that reference empty cells (non-existent in the input)', function() {
  it('Should return #ERR for a1', function() {
    var cells = {
      a: { 
        1: { expr: 'a2 4 + b1 -' },
        2: { expr: '5' }
      }
    };
    traverseCells(cells, 'a', 1);
    assert.equal(cells.a['1'].result, '#ERR');
    assert.equal(cells.a['2'].result, 5);
  });
});


describe('Traverse cells that have valid values', function() {
  var cells = {
    a: {
      1: { expr: 'a2 4 +' },
      2: { expr: '5' }
    },
    b: {
      1: { expr: '2 b2 3 * -' },
      2: { expr: '5' }
    }
  };
  traverseCells(cells, 'a', 1);
  traverseCells(cells, 'b', 1);

  it('Should return 9 for a1', function() {
    assert.equal(cells.a['1'].result, 9);
  });
  it('Should return 5 for a2', function() {
    assert.equal(cells.a['2'].result, 5);
  });
  it('Should return -13 for b1', function() {
    assert.equal(cells.b['1'].result, -13);
  });
  it('Should return 5 for b2', function() {
    assert.equal(cells.b['2'].result, 5);
  });
});

// --------- process-csv tests ------------


describe('Load CSV using test file and confirm successful cells object generation', function() {
  var cells = {};
  async.waterfall([
    function(cb) {
      loadCSV('./test/test.csv', cells, cb);
    },
    function(cells, cb) {
      writeCSV('./test/out.csv', cells, cb);
    }
  ], function (err, cells) {
  });

  it('Should return "-13 5 +" for a1.expr', function() {
    assert.equal(cells.a['1'].expr, '-13 5 +');
    assert.equal(cells.a['1'].result, -8);
  });
  it('Should return -13 for b1.result', function() {
    assert.equal(cells.b['1'].result, -13);
  });
  it('Should return #ERR for d1.result', function() {
    assert.equal(cells.d['1'].result, '#ERR');
  });
  it('Should return 3.5 for d2.result', function() {
    assert.equal(cells.d['2'].result, 3.5);
  });
  it('writeCSV output should match expected output', function() {
    var outStr = fs.readFileSync('./test/out.csv').toString();
    var expectedStr = fs.readFileSync('./test/expected-output.csv').toString();
    assert.equal(outStr, expectedStr);
  });
});

